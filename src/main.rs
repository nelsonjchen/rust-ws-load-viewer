extern crate iron;
extern crate staticfile;
extern crate mount;
extern crate multiqueue;
extern crate tungstenite;

// This example serves the docs from target/doc/staticfile at /doc/
//
// Run `cargo doc && cargo run --example doc_server`, then
// point your browser to http://127.0.0.1:3000/doc/

use std::net::{TcpListener, TcpStream};
use std::path::Path;
use std::{thread, time};


use tungstenite::{Message, connect};


use iron::Iron;
use staticfile::Static;
use mount::Mount;

fn main() {
    println!("Creating Bus");
    let (send, recv) = multiqueue::broadcast_queue(3);

    println!("Creating Threads");
    let mut threads = vec![];
    threads.push(thread::spawn(serve_static));

    let ws_recv = recv.clone();
    threads.push(thread::spawn(move || {
        /// A WebSocket echo server
        let server = TcpListener::bind("127.0.0.1:9001").unwrap();
        for stream in server.incoming() {

            let ws_inst_t = ws_recv.clone();
            thread::spawn(move || {
                let mut websocket = tungstenite::server::accept(stream.unwrap()).unwrap();
                loop {
                    //
                    let v: Result<&str, &str> = ws_inst_t.recv().or(Ok("nothin"));
                    println!("Recv!");

                    websocket
                        .write_message(Message::Text(v.unwrap().to_string()))
                        .unwrap();
                }
            });
        }
    }));
    recv.unsubscribe();

    threads.push(thread::spawn(move || loop {
        println!("Send!");
        let _ = send.try_send("Lol!");
        thread::sleep(time::Duration::from_millis(1000));
    }));


    for thread in threads {
        thread.join().unwrap();
    }
}

fn serve_static() {
    let mut mount = Mount::new();

    // Serve the shared JS/CSS at /
    mount.mount("/", Static::new(Path::new("static/")));

    println!("http://localhost:3000/");

    Iron::new(mount).http("127.0.0.1:3000").unwrap();
}
